## Dynamic Relaxation Workshop


The code (version 1.1) is published under an MIT Licence on [Zenodo](https://zenodo.org/record/3459223#.XYn1APkzZPY) (an IPython Jupyter Notebook was added)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3459172.svg)](https://doi.org/10.5281/zenodo.3459172)

The code (version 1.0) is published under an MIT Licence on [Zenodo](https://zenodo.org/record/3459172#.XYnp4PkzZPY)
[![DOI](https://zenodo.org/badge/210559564.svg)](https://zenodo.org/badge/latestdoi/210559564)