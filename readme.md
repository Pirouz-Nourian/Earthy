# Note: moved to GitHub
This repository is nearly obsolete and is only kept as a legacy. The new repositories are all moved to GitHub and listed [here](https://genesis-lab.dev/courses/earthy/). 
This course is managed and maintained by [Genesis Lab](https://www.tudelft.nl/en/architecture-and-the-built-environment/research/research-facilities/genesis-lab) from 2021 onwards. 

# EARTHY: Computational Design for Earth Architecture

EARTHY is a master’s level design studio with the aim of designing and engineering earthy buildings, in particular adobe buildings, intended for mid-term accommodation of displaced communities. Our goal is to design buildings that can be ideally built by their prospective inhabitants. Earthy buildings are virtually 100% recyclable and, compared to tents, they offer much more comfort. The use of earthen materials necessitates the knowledge of complex geometry e.g. in designing and technical drawing of vaults, domes and arches in optimal shapes. The focus of the course is on the relations of materials, forms, and structures, explored computationally. Automated construction design and generation of assembly instructions are extra challenges to be tackled via computation. 

# Learning Goals:
After finishing this course, the student is expected to be able:
*	to analyse the urban context, social-spatial structure, and vernacular traditions and develop an idea and a concept for the design responding to the context.
*	to computationally design, develop algorithms and underpin the architectural configuration of a settlement suitable for mass-customization in a circular construction process with low-cost materials, local labour, and low-tech construction techniques.
*	to optimize complex geometric forms for a desired structural performance, given a local material and functional requirements.

# Design Challenge:

You are requested to design for betterment of a district of the [Al Zaatari Refugee Camp in Jordan](https://en.wikipedia.org/wiki/Zaatari_refugee_camp).  The course is prepared for 40 students working in 8 groups of 5. The whole class is supposed to propose one Master Plan (Configuration) and a set of Urban Design Guidelines for a displaced-community, plan and design 5 communal buildings (a community centre, a health centre, a school, a marketplace, a public bath) and design 3 types of dwellings for 3 types of families (small, medium, large). These types of buildings must be designed, other types of buildings can be proposed by students. In short, there will be at least 8 sorts of buildings to be configured and designed, each of which is assigned to a single individual/group. The entire class is responsible for the configuration of one district of the site. 


![1](https://user-images.githubusercontent.com/7803528/48273031-559a1000-e440-11e8-848c-b35554cb2ab7.png)
![2](https://user-images.githubusercontent.com/43637877/48062818-6b4ed180-e1c3-11e8-8b1f-83ecca966437.png)
Image Credit: Amey Thakur, Jiahui Cai, Krittika Agarwal , Pierre Kauter, Soujanya K Prasad 
